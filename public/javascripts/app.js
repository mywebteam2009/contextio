var app=angular.module('app',[]);
app.controller('appController',function($scope,$http){

});
app.controller('newUserController',function($scope,$http){
    $scope.newUser={};
    $scope.createNewUser=function(){
        $http.post("/add-mailbox",$scope.newUser).then(function(res){
            alert("success "+res.data.success);
            $scope.getUsers();
        });
    }
    $scope.getUsers=function(){
        $http.get("/mailbox").then(function(res){
                $scope.oldUserList=res.data;
        });
    }
    $scope.getLabel=function(user){
        $http.post("/mailbox-labels",user).then(function(res){
                $scope.labelList=res;
        });
    }
    $scope.getMessages=function(label,config){
        $http.post("/label-messages",{name:label.name,config:config.data}).then(function(res){
                $scope.messagesList=res;
        });
    }
    $scope.openMessages=function(message,config){
        $http.post("/messages",{message:message,config:config.data}).then(function(res){
                $scope.messagesData=res;
        });
    }
    $scope.getUsers();
});