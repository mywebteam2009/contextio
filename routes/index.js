var bodyParser = require('body-parser');
var ContextIO = require('contextio-lite');
var express = require('express');
var router = express.Router();
var ctxio = new ContextIO.Client('lite', 'https://api.context.io', {
  key: "odyphyh6",
  secret: "ViTKW1jNgZVGf6qW"
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Context.io' });
});

router.post('/add-mailbox',function (req, res) {
    if(req.body.email&&req.body.username&&req.body.password){
        var newUser={"email":req.body.email,"username":req.body.username,"password":req.body.password,
                     "server":"eat.home.pl","use_ssl":1,"port":993,"type":'IMAP',};
        ctxio.users('589b363833cf3f3ac37e9204').email_accounts().post(newUser, function (err, response) {
            if (err) throw res.send(err);
            res.send(response.body);
        });
    }else{
        res.send("Plz Enter The Required Name Email Password");
    }
});

router.get('/mailbox',function (req, res) {
    ctxio.users('589b363833cf3f3ac37e9204').email_accounts().get({limit:15}, function (err, response) {
        if (err) throw res.send(err);
        res.send(response.body);
    });
});

router.post('/mailbox-labels',function (req, res) {
    ctxio.users('589b363833cf3f3ac37e9204').email_accounts(req.body.label).folders().get({limit:15}, function (err, response) {
        if (err) throw res.send(err);
        res.send(response.body);
    });
});

router.post('/label-messages',function (req, res) {
    ctxio.users('589b363833cf3f3ac37e9204').email_accounts(req.body.config.label)
        .folders(req.body.name).messages().get({limit:15}, function (err, response) {
        if (err) throw res.send(err);
        res.send(response.body);
    });
});

router.post('/messages',function (req, res) {
    ctxio.users('589b363833cf3f3ac37e9204').email_accounts(req.body.config.config.label)
        .folders(req.body.config.name).messages(req.body.message.message_id).get({limit:15}, function (err, response) {
        if (err) throw res.send(err);
        res.send(response.body);
    });
});

module.exports = router;
